<?php

function specialSum($elements)
{
    $count = 0;
    if (count($elements) % 2 == 0 ) {
        foreach ($elements as $element => $value) {
            if ($element % 2 == 0) {
                $count = $count + $value;
            }
        }
    } else {
        foreach ($elements as $element => $value) {
            if ($element % 2 == 1) {
                $count = $count + $value;
            }
        }
    }
    return $count;
}

function factorielle($n) {
    $count = 1;
    for ($i = 1 ; $i <= $n ; $i++ ){
        $count = $count * $i;
    }
    return $count;
}

