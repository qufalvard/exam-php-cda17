<?php
include('includes/config.inc.php');

if (!empty($_POST['subject']) && !empty($_POST['message'])) {
    $subject = $_POST['subject'];
    $message = $_POST['message'];

    $sql = "INSERT INTO contact (subject, message) VALUES (:subject, :message)";
    $statement = $connection->prepare($sql);
    $statement->bindParam(':subject', $subject, PDO::PARAM_STR);
    $statement->bindParam(':message', $message, PDO::PARAM_STR);
    $statement->execute();

    echo 'Ajout du message bien effectué';
}

$sql = 'SELECT message, subject FROM contact';
$results = $connection->query($sql);
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table>
    <thead>
    <tr>
        <th>Sujet</th>
        <th>Message</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($results->fetchAll(PDO::FETCH_ASSOC) as $result) { ?>
        <tr>
            <td><?= $result['subject'] ?></td>
            <td><?= $result['message'] ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

</body>
</html>

