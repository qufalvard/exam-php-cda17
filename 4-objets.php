<?php

spl_autoload_register(function ($class) {
    require_once "classes/$class.php";
});

$polo = new Character();
$polo->setName('Polo');
$polo->setAttack(10);
$polo->setDefense(20);

$poto = new Character();
$poto->setName('Poto');
$poto->setAttack(20);
$poto->setDefense(10);

echo $polo->getName() . '<br>';
echo $polo->getAttack() . '<br>';
echo $polo->getDefense() . '<br>';
echo '<br>';
echo $poto->getName() . '<br>';
echo $poto->getAttack() . '<br>';
echo $poto->getDefense() . '<br>';
