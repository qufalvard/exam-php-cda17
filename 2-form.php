<?php
session_start();

if (!empty($_POST['email']) && !empty($_POST['text'])) {
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == true) {
        echo trim($_POST['email']) . "<br>" .trim($_POST['text']) . "<br>";
        echo '<a href="2-superglobales.php?value=test">Lien vers l exercice suivant</a>';
    } else {
    }
}

?>

<form method="post">
    <div>
        <label for="email">
            Email
        </label>
        <input id="email" type="" name="email" required>
    </div>
    <div>
        <label for="text">
            Un texte
        </label>
        <input id="text" type="text" name="text" required>
    </div>
    <button type="submit">Valider</button>
</form>
