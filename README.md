# Examen PHP

Pour travailler sur ce projet : 
- **créer un fork** du projet (sur la page [https://gitlab.com/drakona-formation/exam-php-cda17](https://gitlab.com/drakona-formation/exam-php-cda17), cliquer sur le bouton `fork`, en haut à droite de la page)
- Cloner **votre** projet (commande `git clone` par exemple)
  - Si vous utilisez Wamp, je vous conseille de le cloner dans le dossier `www` de Wamp, vous allez exécuter du code PHP
- Créer une branche pour faire tout l'examen
- À la fin de l'examen, vous **devez** envoyer un zip de votre code sur Moodle et vous **pouvez** faire une PR à destination du projet d'origine (afin de faciliter mes retours pour la correction)

La durée prévue est d'environ 3h. Des points peuvent être perdus pour le retard du rendu :
- 1 point si le rendu est fait après 13h30
- 2 point si le rendu est fait après 15h

Liste des exercices
1. Fonctions
2. Création d'un formulaire
3. Base de données
4. Objets
5. Trouver les problèmes et les résoudre

## 1. Fonctions

- Dans `1-fonctions.php` :
  - Créer une fonction `specialSum` qui 
    - [ ] prend un tableau `elements` en paramètre
    - [ ] renvoie la somme des éléments avec un index pair (`index % 2 == 0`) si le nombre d'éléments est impair
    - [ ] sinon, renvoie la somme des éléments avec un index impair (`index % 2 == 1`)
  - Les appels dans `index.php` doit fonctionner (il manque quelque chose ;) )
    
  - Créer une fonction `factorielle` qui 
    - [ ] prend un nombre entier `n` en paramètre
    - [ ] renvoie la multiplication des entiers de 1 à `n` (`1 * 2 * 3 * ... * n`)
  - Les appels dans `index.php` doit fonctionner (il manque quelque chose ;) )

- (optionnel) Créer un commit avec vos changements
  
## 2. Création d'un formulaire

- Dans `2-form.php` :
  - [ ] Corriger les erreurs dans le HTML
  - [ ] Le formulaire doit envoyer des données `POST`
  - [ ] Gérer la soumission du formulaire et valider les données
    - [ ] Les deux champs sont requis et ne doivent pas être vides (utiliser la fonction `trim()` avant la vérification)
    - [ ] Le champ `email` doit contenir une adresse email valide (`filter_var` ([dont la documentation est ici](https://www.php.net/manual/fr/function.filter-var)))
    - [ ] Si les données sont valides, les afficher avec des `echo` ou `var_dump` et afficher ce lien :

```html
<a href="2-superglobales.php?value=test">Lien vers l'exercice suivant</a>
```

- Dans `2-superglobales.php` :
  - [ ] Récupérer la valeur de `value` contenu dans l'url.
  - [ ] Vérifier que `value` n'est pas vide et en mettre la valeur dans l'index `price` de la session.

- (optionnel) Créer un commit avec vos changements
  
## 3. Base de données

- [ ] Ouvrir [PhpMyAdmin](http://localhost/phpmyadmin/) et créer une base de données `php-exam`
- [ ] Créer la table `contact` avec les champs suivants
    - [ ] `id` (entier, auto-incrementé)
    - [ ] `subject` (varchar, taille de 255 caractères)
    - [ ] `message` (text)
- [ ] Créer un fichier `includes/config.inc.php` créant la connexion à la base de données
- Dans `2-superglobales.php`, modifier l'action du formulaire pour soumettre le formulaire dans la page `3-form-bdd.php` (où vous allez traiter les données `POST`)
- Dans `3-form-bdd.php`,
  - [ ] Inclure le fichier de configuration
  - [ ] À la soumission du formulaire, si les données ne sont pas vides
    - [ ] Utiliser PDO pour faire une requête `INSERT INTO` pour ajouter les données dans la base
  - [ ] Récupérer les données de la table `contact` (grâce à PDO) et les afficher dans un tableau HTML `table`

- (optionnel) Créer un commit avec vos changements

## 4. Objets

- Dans le dossier `classes`
  - Créer une classe `Character` dans son propre fichier avec les propriétés suivantes (définies en `protected`)
    - [ ] `name` de type `string`
    - [ ] `attack` de type `int`
    - [ ] `defense` de type `int`
  - [ ] Écrire les getters et les setters de ces propriétés

- Dans `4-objets.php` :
  - [ ] Instancier 2 objets de type `Character` avec des valeurs distinctes
  - [ ] afficher les valeurs des propriétés avec des `echo`

- (optionnel) Créer un commit avec vos changements

## 5. Trouver les problèmes et les résoudre

- Plusieurs erreurs sont présentes dans le fichier `5-debug.php`
  - [ ] Les corriger et vérifier le fonctionnement du script

- (optionnel) Créer un commit avec vos changements
