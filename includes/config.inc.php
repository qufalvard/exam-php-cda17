<?php
$dsn = 'mysql:host=localhost;dbname=php-exam;charset=utf8;port=3307';
$user = 'root';
$password = '';

try {
    $connection = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    exit('Connexion échouée : ' . $e->getMessage());
}
